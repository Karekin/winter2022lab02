public class MethodsTest {
	
	public static void main(String[] args){
		
		int x = 10;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x + 50);
		
		methodTwoInputNoReturn(3, 7.5);
		
		int z = methodNoInputReturnInt();
		System.out.println(z);
		
		double y = sumSquareRoot(6, 3);
		System.out.println(y);
		
		String s1 = "hello";
		String s2 = "goodbye";
		
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		SecondClass sc = new SecondClass();
		System.out.println(SecondClass.addOne(50));
		System.out.println(sc.addTwo(50));
	}
	
	public static void methodNoInputNoReturn(){
		
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 50;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int z){
		
		System.out.println("Inside the method one input and no return" + " " + z);
		
	}
	
	public static void methodTwoInputNoReturn(int z, double d){
		
		System.out.println(z + " " + d);
	}
	
	public static int methodNoInputReturnInt(){
		
		return 6;
	}
	
	public static double sumSquareRoot(int x, int z){
		
		double y = x + z;
		y = Math.sqrt(y);
		return y;
	}
}