import java.util.Scanner;
public class PartThree {
	
	public static void main(String[] args){
		Scanner s = new Scanner(System.in);
		AreaComputations area = new AreaComputations();
		
		System.out.println("Square side");
		int side = s.nextInt();
		
		System.out.println("Rectangle width");
		int width = s.nextInt();
		System.out.println("Rectangle length");
		int length = s.nextInt();
		
		System.out.println("Area of Square: ");
		System.out.println(AreaComputations.areaSquare(side));
		
		System.out.println("Area of Rectangle");
		System.out.println(area.areaRectangle(width, length));
	}
}