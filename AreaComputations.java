public class AreaComputations {
	
	public static int areaSquare(int side){
		
		return side * side;
	}
	
	public int areaRectangle(int length, int width){
		
		return length * width;
	}
}